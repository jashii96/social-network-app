from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from django.utils import timezone
from django.db.models import Q

from .models import User, FriendRequest, Friend
from .serializers import UserSerializer,UserListSerializer, LoginSerializer,FriendRequestSerializer, FriendSerializer


# Create your views here.
class SignupAPIView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            cleaned_data = serializer.data
            user.set_password(cleaned_data["password"])
            user.save()
            return Response(
                {"status": True, "message": "Registration Successful","data": None},
                status= status.HTTP_200_OK
            )
        return Response(
                {"status": False, "message": serializer.errors,"data": None},
                status= status.HTTP_400_BAD_REQUEST
            )

class LoginAPIView(APIView):
    model_class = User

    @staticmethod
    def __generate_token(user):
        refresh = RefreshToken.for_user(user)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }
    
    @staticmethod
    def __authenticate(cleaned_data):
        try:
            user = User.objects.get(email=cleaned_data["email"].lower())
        except User.DoesNotExist:
            return False
        _checked = user.check_password(cleaned_data["password"])
        if _checked:
            return user
        else:
            return False

    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            cleaned_data = serializer.data
            user = self.__authenticate(cleaned_data)
            if user:
                # generate token here
                generated_token = self.__generate_token(user)
                return Response(
                {"status": True, "message": "Login Successful","data": {**generated_token, "id":user.id}},
                status= status.HTTP_200_OK
            )
           
        return Response(
                {"status": False, "message": serializer.errors,"data": None},
                status= status.HTTP_400_BAD_REQUEST
            )

class UserSearchAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserListSerializer
    queryset = User.objects.all()

    def get_queryset(self):
        keyword = self.request.query_params.get('q', None)
        if keyword:
            # Check if keyword is an exact email
            if '@' in keyword:
                return User.objects.filter(email=keyword)
            # Otherwise, search by name containing the keyword
            else:
                return User.objects.filter(name__icontains=keyword)
        return User.objects.none()

class SendFriendRequestAPIView(APIView):
    model_class = FriendRequest
    def post(self, request):
        # Check rate limit
        user = request.user

        #  top record should be latest 
        last_3_req = self.model_class.objects.filter(from_user=request.user).order_by('-id')[:3]
        try:
            if (timezone.now() - last_3_req[2].created_at).total_seconds() < 60:
                return Response({"status": False,"message": "You have exceeded the limit of friend requests. Try again later."}, status=status.HTTP_429_TOO_MANY_REQUESTS)
        except:
            #  user might not send 3 requests till now
            pass
        
        if self.model_class.objects.filter(from_user_id = to_user_id, to_user = request.user).exists():
            return Response({"status": False,"message": "User Already send friend requests. "})

        # Handle sending friend request
        to_user_id = request.data.get('to_user_id', 0)
        try:
            to_user = User.objects.get(id=to_user_id)
        except User.DoesNotExist:
            return Response({"status": False,"message": "User does not exist."}, status=status.HTTP_400_BAD_REQUEST)

        if user == to_user:
            return Response({"status": False, "message": "You can't send a friend request to yourself."}, status=status.HTTP_400_BAD_REQUEST)

        if user.sent_requests.filter(to_user=to_user).exists():
            return Response({"status": False, "message": "Friend request already sent."}, status=status.HTTP_400_BAD_REQUEST)

        FriendRequest.objects.create(from_user=user, to_user=to_user, status='Pending')
        return Response({"status": True, "message": "Friend request sent successfully."}, status=status.HTTP_201_CREATED)

class AcceptFriendRequestAPIView(APIView):
    def post(self, request):
        # Handle accepting friend request
        friend_request_id = request.data.get('friend_request_id')
        try:
            friend_request = FriendRequest.objects.get(id=friend_request_id, to_user=request.user, status='Pending')
        except FriendRequest.DoesNotExist:
            return Response({"status": False, "message": "Friend request not found or already accepted/rejected."}, status=status.HTTP_400_BAD_REQUEST)

        friend_request.status = 'Accepted'
        friend_request.save()
        Friend.objects.create(user=request.user, friend=friend_request.from_user)
        return Response({"status": True, "message": "Friend request accepted successfully."}, status=status.HTTP_200_OK)

class RejectFriendRequestAPIView(APIView):
    def post(self, request):
        # Handle rejecting friend request
        friend_request_id = request.data.get('friend_request_id')
        try:
            friend_request = FriendRequest.objects.get(id=friend_request_id, to_user=request.user, status='Pending')
        except FriendRequest.DoesNotExist:
            return Response({"status": False, "message": "Friend request not found or already accepted/rejected."}, status=status.HTTP_400_BAD_REQUEST)

        friend_request.status = 'Rejected'
        friend_request.save()
        return Response({"status": True, "message": "Friend request rejected successfully."}, status=status.HTTP_200_OK)

class FriendsListAPIView(generics.ListAPIView):
    serializer_class = FriendSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Friend.objects.filter(Q(user=self.request.user) | Q(friend=self.request.user))

class FriendRequestsListAPIView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = FriendRequestSerializer

    def get_queryset(self):
        keyword = self.request.query_params.get('q', None)
        if keyword.lower() == "sent":
            return FriendRequest.objects.filter(from_user=self.request.user, status="Pending")
        else:
            return FriendRequest.objects.filter(to_user=self.request.user, status="Pending")


