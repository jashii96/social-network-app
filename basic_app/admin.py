from django.contrib import admin
from .models import User,FriendRequest, Friend

# Register your models here.
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'name') 
admin.site.register(User, UserAdmin)

class FriendRequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'from_user', 'to_user', 'status', 'created_at') 
admin.site.register(FriendRequest, FriendRequestAdmin)

class FriendAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'friend', 'created_at') 
admin.site.register(Friend, FriendAdmin)
