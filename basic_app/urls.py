from django.urls import path
from .views import (SignupAPIView,
    LoginAPIView,
    UserSearchAPIView, 
    SendFriendRequestAPIView,
    AcceptFriendRequestAPIView,
    RejectFriendRequestAPIView,
    FriendRequestsListAPIView,
    FriendsListAPIView
    )

urlpatterns = [
    path('signup/', SignupAPIView.as_view(), name='signup'),
    path('login/', LoginAPIView.as_view(), name='login'),
    path('search/', UserSearchAPIView.as_view(), name='user-search'),
    path('send-request/', SendFriendRequestAPIView.as_view(), name='send_friend_request'),
    path('accept-request/', AcceptFriendRequestAPIView.as_view(), name='accept_friend_request'),
    path('reject-request/', RejectFriendRequestAPIView.as_view(), name='reject_friend_request'),
    path('friends-requests-list/', FriendRequestsListAPIView.as_view(), name='list_friends_requests'),
    path('friend-list/', FriendsListAPIView.as_view(), name='friends_list'),
]
