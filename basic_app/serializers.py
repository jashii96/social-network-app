from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.core.validators import MaxLengthValidator
from .models import User, FriendRequest, Friend

def check_user_exists(email):
    """
    if user exists then return false
    """
    if not User.objects.filter(email=email).exists(): 
        return email.lower()
    raise ValidationError("Sorry! This E-Mail has been already taken.")

class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True, validators = [check_user_exists])
    password = serializers.CharField(validators=[MaxLengthValidator(20)] , required=True)
    name  = serializers.CharField(validators=[MaxLengthValidator(50)], required=True)
    class Meta:
        model = User
        fields = ['id','email', 'password', 'name']

class LoginSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','email', 'name']

class FriendRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = FriendRequest
        fields = ['id', 'from_user', 'to_user', 'status', 'created_at']


class FriendRequestSerializer(serializers.ModelSerializer):
    from_user = UserSerializer()
    to_user = UserSerializer()

    class Meta:
        model = FriendRequest
        fields = '__all__'

class FriendSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    friend = UserSerializer()

    class Meta:
        model = Friend
        fields = '__all__'