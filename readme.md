# Application Setup Guide

# Before running the application, ensure that you have Python 3.8 or later installed on your system.
# Make Sure You have install virtual enviroment in you system then run step 1
# Make sure to replace virtual_environment_name with your desired name.
# Substitute app_name with the actual name of your Django app.
# Modify the host and port numbers as per your requirements.
# Refer to the Postman collection for detailed API documentation and examples.
# Using SQLite Database 
# 

Step 1: Set up Virtual Environment
- Open your terminal.
- Navigate to the project directory.
- Execute the following command to create a virtual environment:

"python3.8 -m venv virtual_environment_name"

Step 2: Build the Model
- After setting up the virtual environment, activate it.
- Execute the following command to build the model:

"python manage.py makemigrations app_name/"

Step 3: Migrate the Model
- Once the model is built, run the migration using the following command:

"python manage.py migrate"

Step 4: Create Super User
- To create a superuser for the application, run the following command and follow the prompts:

"python manage.py createsuperuser"

- Enter your email and set a password.

Step 5: Run the Application
Start the application by executing the following command:

"python manage.py runserver"

- To specify a custom host and port, use the following command:

"python manage.py runserver 0.0.0.0:8000"

Step 6: API Testing with Postman
- Utilize the provided Postman collection to interact with the application's APIs. Execute the following actions:

- Registration
- Login
- Search User
- Send Friend Request
- Accept Friend Request
- Reject Friend Request
- Friend Request List
- Approve Friend List

scope for optimaztion:

1) if user A sends friend request to user B. still user B is able to send a friend request back to user A, 
resulting relationship between two users might get affected as A might accept and B might reject the request.

proposed solution: if A have sent a friend request to B already, while B is trying to send a request back to A, a response/notification should be sent to B such as a request from user A is in pending. 
